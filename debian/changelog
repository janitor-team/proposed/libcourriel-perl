libcourriel-perl (0.49-1) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/copyright: replace tabs with spaces / remove trailing
    whitespace.

  [ Debian Janitor ]
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * Update standards version to 4.5.0, no changes needed.
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on
      libdatetime-format-mail-perl.
    + libcourriel-perl: Drop versioned constraint on
      libdatetime-format-mail-perl in Depends.

  [ gregor herrmann ]
  * Import upstream version 0.49.
  * Update debian/upstream/metadata.
  * Update years of upstream and packaging copyright.
  * Declare compliance with Debian Policy 4.6.0.
  * Set Rules-Requires-Root: no.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Sun, 22 Aug 2021 22:48:04 +0200

libcourriel-perl (0.48-1) unstable; urgency=medium

  [ Laurent Baillet ]
  * fix lintian wrong-path-for-interpreter error

  [ Xavier Guimard ]
  * Email change: Xavier Guimard -> yadd@debian.org

  [ gregor herrmann ]
  * Import upstream version 0.48.
  * Update (build) dependencies.
  * Install new upstream Code of Conduct document.
  * Update years of upstream and packaging copyright.
  * Annotate test-only build dependencies with <!nocheck>.
  * Declare compliance with Debian Policy 4.4.0.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.
  * Remove trailing whitespace from debian/*.

 -- gregor herrmann <gregoa@debian.org>  Fri, 19 Jul 2019 00:26:39 -0300

libcourriel-perl (0.47-1) unstable; urgency=medium

  * Import upstream version 0.47
  * Replace issue tracking link by GitHub

 -- Xavier Guimard <x.guimard@free.fr>  Tue, 22 May 2018 06:39:26 +0200

libcourriel-perl (0.46-1) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Xavier Guimard ]
  * Import upstream version 0.46 (Closes: #887537)
  * Declare conformance with Policy 4.1.4 (no changes needed)
  * Bump debhelper compatibility level to 10
  * Update debian/copyright years
  * Replace libemail-address-perl and libemail-address-list-perl by
    libemail-address-xs-perl in dependencies
  * Add libtest-warnings-perl in build-dependencies

 -- Xavier Guimard <x.guimard@free.fr>  Mon, 14 May 2018 06:44:42 +0200

libcourriel-perl (0.45-1) unstable; urgency=medium

  * Import upstream version 0.45.
  * Update years of upstream and packaging copyright.
  * Update (build) dependencies.
  * Declare compliance with Debian Policy 4.1.2.

 -- gregor herrmann <gregoa@debian.org>  Sat, 23 Dec 2017 21:08:58 +0100

libcourriel-perl (0.44-1) unstable; urgency=medium

  * Import upstream version 0.44.
  * Update URLs in debian/upstream/metadata.
  * Update (build) dependencies.

 -- gregor herrmann <gregoa@debian.org>  Wed, 28 Dec 2016 20:46:25 +0100

libcourriel-perl (0.42-1) unstable; urgency=medium

  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.
  * Import upstream version 0.42.
  * Make (build) dependency on libdatetime-format-mail-perl versioned.
  * Install new CONTRIBUTING document.

 -- gregor herrmann <gregoa@debian.org>  Wed, 20 Jul 2016 19:30:07 +0200

libcourriel-perl (0.41-1) unstable; urgency=medium

  * Import upstream version 0.41.
  * Declare compliance with Debian Policy 3.9.8.
  * Drop unneeded version constraints and alternative build dependencies.

 -- gregor herrmann <gregoa@debian.org>  Thu, 28 Apr 2016 17:19:20 +0200

libcourriel-perl (0.40-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Rename autopkgtest configuration file(s) as per new pkg-perl-
    autopkgtest schema.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Import upstream version 0.40.
  * Update years of upstream and packaging copyright.
  * Add (build) dependency on libemail-address-list-perl.
  * Declare compliance with Debian Policy 3.9.7.

 -- gregor herrmann <gregoa@debian.org>  Fri, 12 Feb 2016 21:18:32 +0100

libcourriel-perl (0.39-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.39
  * Bump debhelper compatibility level to 9

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Thu, 13 Aug 2015 10:09:21 -0300

libcourriel-perl (0.37-2) unstable; urgency=medium

  * Fix autopkgtest.
    Exclude a file from the syntax test. (Closes: #785749)

 -- gregor herrmann <gregoa@debian.org>  Sun, 24 May 2015 12:28:36 +0200

libcourriel-perl (0.37-1) unstable; urgency=medium

  * Import upstream version 0.37
  * Update years of upstream and packaging copyright.
  * Declare compliance with Debian Policy 3.9.6.
  * Mark package as autopkgtest-able.
  * Update build and runtime dependencies.

 -- gregor herrmann <gregoa@debian.org>  Sat, 09 May 2015 22:59:53 +0200

libcourriel-perl (0.36-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata
  * Imported upstream version 0.36

 -- gregor herrmann <gregoa@debian.org>  Mon, 15 Sep 2014 19:45:26 +0200

libcourriel-perl (0.35-1) unstable; urgency=medium

  * New upstream release.
  * debian/control: replace libfile-slurp-perl with
    libfile-slurp-tiny-perl.

 -- gregor herrmann <gregoa@debian.org>  Tue, 13 May 2014 19:56:03 +0200

libcourriel-perl (0.34-1) unstable; urgency=medium

  * New upstream release.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Sun, 11 May 2014 16:09:19 +0200

libcourriel-perl (0.33-1) unstable; urgency=medium

  * Team upload

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Florian Schlichting ]
  * Import Upstream version 0.33
  * Bump year of upstream copyright
  * Declare compliance with Debian Policy 3.9.5

 -- Florian Schlichting <fsfs@debian.org>  Tue, 22 Apr 2014 23:20:32 +0200

libcourriel-perl (0.31-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Xavier Guimard ]
  * Imported Upstream version 0.31
  * Add eg/* in example files
  * Add version to liblist-moreutils-perl dependency
  * Update debian/copyright years

 -- Xavier Guimard <x.guimard@free.fr>  Fri, 05 Apr 2013 05:55:00 +0200

libcourriel-perl (0.30-1) unstable; urgency=low

  * New upstream version
  * Add myself to uploaders and copyright
  * Bump Standards-Version to 3.9.4
  * Update debian/NEWS

 -- Xavier Guimard <x.guimard@free.fr>  Mon, 24 Sep 2012 06:53:45 +0200

libcourriel-perl (0.29-1) unstable; urgency=low

  * Team upload.
  * Imported Upstream version 0.29
  * Update debian/copyright information.
    Update format to copyright-format 1.0 as released together with Debian
    policy 3.9.3 (only URL change needed).
  * Bump Standards-Version to 3.9.3

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 13 Mar 2012 08:16:16 +0100

libcourriel-perl (0.28-1) unstable; urgency=low

  * Team upload.
  * Imported Upstream version 0.28
  * Add libemail-abstract-perl as dependency.
    Add libemail-abstract-perl to (Build-)Depends(-Indep) fields in
    debian/control file.
  * Change dependency on liblist-moreutils-perl.
    Change versioned (Build-)Depends(-Indep) on liblist-moreutils-perl to a
    unversioned dependency as the required version is satisfied in Squeeze
    and Lenny (oldstable) is end-of-life.
  * Simplify dependencies satisfied in Stable.
    Drop alternate (Build-)Depends(-Indep) for perl (>= 5.10.1) |
    libtest-simple-perl (>= 0.88) and perl (>= 5.10.1) | libparent-perl as
    they are already satisfied in Squeeze.
  * Update copyright years for upstream files

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 07 Feb 2012 21:36:48 +0100

libcourriel-perl (0.27-1) unstable; urgency=low

  * Team upload.

  [ gregor herrmann ]
  * New upstream release. (0.26)

  [ Salvatore Bonaccorso ]
  * Imported Upstream version 0.27
  * Document API changes in debian/NEWS.
    Document API changes in 0.27

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 26 Sep 2011 15:21:59 +0200

libcourriel-perl (0.25-1) unstable; urgency=low

  * New upstream release.
  * Add debian/NEWS about API changes.
  * Update build and runtime dependencies.

 -- gregor herrmann <gregoa@debian.org>  Sat, 17 Sep 2011 16:51:26 +0200

libcourriel-perl (0.19-1) unstable; urgency=low

  [ Harlan Lieberman-Berg ]
  * Team upload.
  * Imported Upstream version 0.19
  * Cleanup d/copyright headers for DEP5 compliance.
  * Remove timezone patch as upstream appears to have fixed the issue.
  * Remove unused series.

  [ gregor herrmann ]
  * Add a build dependency on libpath-class-perl to (re-)enable additional
    tests.

 -- Harlan Lieberman-Berg <H.LiebermanBerg@gmail.com>  Mon, 22 Aug 2011 14:45:33 -0400

libcourriel-perl (0.18-1) unstable; urgency=low

  * Team upload
  * New upstream release
    * Add new (build-)dependency libdatetime-format-natural-perl
    * Refresh timezone.patch

 -- Maximilian Gass <mxey@cloudconnected.org>  Sun, 21 Aug 2011 15:42:47 +0200

libcourriel-perl (0.17-1) unstable; urgency=low

  * Team upload

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ Maximilian Gass ]
  * New upstream release

  [ gregor herrmann ]
  * Refresh patch (offset).

 -- Maximilian Gass <mxey@cloudconnected.org>  Sat, 20 Aug 2011 13:52:50 +0200

libcourriel-perl (0.16-1) unstable; urgency=low

  * Team upload.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Imported Upstream version 0.16

 -- Salvatore Bonaccorso <carnil@debian.org>  Thu, 04 Aug 2011 08:18:52 +0200

libcourriel-perl (0.15-1) unstable; urgency=low

  * Team upload.
  * New upstream release
  * Refresh timezone.patch (offsets).

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 01 Jul 2011 20:29:24 +0200

libcourriel-perl (0.13-1) unstable; urgency=low

  [ Maximilian Gass ]
  * New upstream release

 -- gregor herrmann <gregoa@debian.org>  Tue, 21 Jun 2011 18:43:06 +0200

libcourriel-perl (0.12-1) unstable; urgency=low

  * Initial release (closes: #629981).

 -- gregor herrmann <gregoa@debian.org>  Fri, 10 Jun 2011 01:22:14 +0200
