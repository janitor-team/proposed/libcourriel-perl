libcourriel-perl (0.30-1) unstable; urgency=low

  API CHANGES in 0.30

  - Courriel::Builder no longer exports a bcc() subroutine. Creating Bcc
    headers in emails does not make any sense whatsoever.

 -- Xavier Guimard <x.guimard@free.fr>  Sun, 30 Sep 2012 05:56:29 +0200

libcourriel-perl (0.27-1) unstable; urgency=low

  API CHANGES in 0.27

  - Renamed the Header->as_header_string and HeaderAttribute->as_header_string
    methods to as_string, so that all objects have the same stringification API.

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 26 Sep 2011 15:18:01 +0200

libcourriel-perl (0.25-1) unstable; urgency=low

  API CHANGES in 0.21

  - The ->get_attribute() method for Courriel::ContentType and ::Disposition
    objects was replaced by ->attribute in 0.20.

  API CHANGES in 0.20

  - The way headers are represented has been changed quite a bit
    internally. Read on for details.

    All headers are now represented as Courriel::Header objects instead of plain
    strings. This means that calling $headers->get('Foo') returns objects. To
    get the value, you need to call ->value on the individual objects.

    See Courriel::Header for the new class for headers.

  - As a corrolary, the Courriel::ContentType and Courriel::Disposition classes
    have been renamed to Courriel::Header::ContentType and
    Courriel::Header::Disposition. They are both now subclasses of
    Courriel::Header. The APIs of these classes have remained mostly the same,
    except for attribute handling (see below).

    The old classes are still shipped in the distro, but they die when
    loaded. This avoids leaving behind old modules which no longer work with
    newer versions of Courriel.

    Methods which returned the old object now return the new one. The
    Courriel::Headers class will return the appropriate object when the header
    is fetched with $headers->get().

  - Header attributes for the Content-Type and Content-Disposition object are
    now represented as Courriel::HeaderAttribute objects. This is necessary
    because these attributes may contain information about their character set
    and language, per RFC2231.

 -- gregor herrmann <gregoa@debian.org>  Sat, 17 Sep 2011 16:35:27 +0200
